---
layout: docs
title: Additional Resources
permalink: /docs/additional
---

## Community
- [Gitlab]({{site.repo}})
- [Mailing List]({{ site.email_list }})
<!--- Slack-->
<!--- Meetings-->


## Videos
- [AI on OpenShift](https://www.youtube.com/watch?v=MD1x2IT7rdg)
- [Fraud Detection using the Open Data Hub](https://youtu.be/IcQ2bhsw_kQ)
- [Data Exploration with JupyterHub on OpenShift](https://www.youtube.com/watch?v=by0l3b55i7g)
- [Building AI with Ceph and OpenShift](https://www.youtube.com/watch?v=B6E7SyxOB2M)
- [Using the Massachusetts Open Cloud Data Hub to perform Data Science Experiments](https://www.youtube.com/watch?v=iUJ6RGfY0JQ)
- [Ceph Object Storage for AI and ML Workloads](https://www.youtube.com/watch?v=n2IW3VIZmg4)
- [Using the Mass Open Cloud to perform Data Science Experiments](https://youtu.be/CZwUCgkKIc4)

## Audio
- [Innovate @Open podcast](https://grhpodcasts.s3.amazonaws.com/opendatahub1908.mp3)
